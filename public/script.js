console.log('beg script!');
if (
  'registerElement' in document
  && 'import' in document.createElement('link')
  && 'content' in document.createElement('template'))
{
  console.log('platform is good!')
} else {
  console.log('polyfill the platform!');
  let e = document.createElement('script');
  e.src = '/webcomponentsjs/webcomponents-lite.min.js';
  document.head.appendChild(e);
}
console.log('end script!');