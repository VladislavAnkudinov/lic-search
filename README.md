# Search for licenses of listed files

## Install and run

#### JRE installation

It is assumed that 
[Java Runtime Environment](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) is installed.
Otherwise please install it. This needed to work with `.jar` archives, since regular
`zip` utilities and libraries may do it with errors.

#### Depencencies installation

In order to install and run project fire 
in your console at the root directory of the project 
the following command

 - `npm run up`
 
To just install all depencencies fire

 - `npm run setup`
 
To run for development
 
 - `npm run watch`
 
## Usage

 - target your browser to localhost:3000
 - put a list of files separated with `\n` (newline) into
textfield and 
 - push `Submit` button
 - wait awhile - this could take some time depending on number of files