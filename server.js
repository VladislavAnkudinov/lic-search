const PORT = 3000;
const MAX_SHIFTS = 2;
const fs = require('fs');
const path = require('path');
const unzip = require('unzip');
const glob = require('glob');
const parseXml = require('xml2js').parseString;
const exec = require('child_process').exec;
const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const rp = require('request-promise');
const fetch = require('node-fetch');
const zlib = require('zlib');
const mkdirp = require('mkdirp');
const app = express();

app.use(express.static('bower_components'));
app.use(express.static('public'));

app.use(bodyParser.json());

const cacheGet = {};

const get = uri => {
  cacheGet[uri] = cacheGet[uri] || rp.get({uri, json: true})
  return cacheGet[uri];
}

const shiftFileName = fileName => fileName.split('.').slice(0, -1).join('.');
const normFileName = fileName => 'fc:' + fileName.split('/').slice(0, -1).join('.');

const getDocsByFileName = fileDoc => {
  const encoded = encodeURIComponent(fileDoc.fileName);
  const uri = 'http://search.maven.org/solrsearch/select?q=' + encoded;
  console.log('uri =', uri);
  return get(uri)
    .then(res => {
      console.log(fileDoc.fileName, 'get(uri) =', res);
      const docs = (res.response || {}).docs || [];
      if (docs.length == 0) {
        if (res.alternate) {
          const alternate = Array.isArray(res.alternate) ? res.alternate[0] : res.alternate;
          fileDoc.alternates = fileDoc.alternates || [];
          if (fileDoc.alternates.indexOf(alternate) == -1) {
            fileDoc.alternates.push(alternate);
            fileDoc.fileName = alternate;
            return getDocsByFileName(fileDoc);
          }
        }
        let fileName = shiftFileName(fileDoc.fileName);
        if (false && fileName) {
          fileDoc.fileName = fileName;
          return getDocsByFileName(fileDoc);
        }
      }
      fileDoc.docs = docs
      return fileDoc;
    });
};

const cacheLic = {};

const getLic = doc => {
  const base = doc.g.split('.').join('/') + '/' + doc.a + '/' + doc.v + '/' + doc.a + '-' + doc.v;
  const href = base + '-sources.jar';
  const pomHref = base + '.pom';
  const remoteContent = 'http://search.maven.org/remotecontent?filepath=';
  const uri = remoteContent + encodeURIComponent(href);
  const pomUri = remoteContent + encodeURIComponent(pomHref);
  console.log('href =', href);
  console.log('pomHref', pomHref);
  console.log('uri =', uri);
  console.log('pomUri =', pomUri);
  cacheLic[href] = cacheLic[href] || new Promise((resolve, reject) => {
      fs.readdir(path.resolve('fetch', href), (err, files) => {
        if (!err && files.length) return console.log('already fetched!') ^ resolve();
        mkdirp(path.resolve('fetch', href), err => {
          if (err) return reject(err);
          fetch(uri)
            .then(res => {
              res.body.pipe(fs.createWriteStream(path.resolve('fetch', href, path.basename(href))))
                .on('close', () => {
                  console.log('unzip close');
                  resolve()
                })
                .on('error', (error) => {
                  console.log('unzip error =', error.message);
                  reject(error);
                })
            })
        });
      });
    })
      .then(() => {
        return new Promise((resolve, reject) => {
          fs.readdir(path.resolve('fetch', href), (err, files) => {
            if (!err && files.length > 1) return console.log('already extracted!') ^ resolve();
            const command = 'cd ' + path.resolve('fetch', href) + ' && jar xf ' + path.basename(href);
            console.log('exec command =', command);
            exec(command, (error, stdout, stderr) => {
              if (error) {
                console.error(`exec error: ${error}`);
                return reject(error);
              }
              resolve();
            });
          });
        });
      })
      .then(() => {
        console.log('unpack success! href =', href);
        console.log('try to find in LICENSE.txt')
        return new Promise((resolve, reject) => {
          fs.readFile(path.resolve('fetch', href, 'META-INF', 'LICENSE.txt'), (error, data) => {
            if (error) return reject(error);
            resolve(data);
          });
        })
          .then(src => {
            src = String(src);
            if (src.indexOf('MIT') !== -1) return 'MIT';
            if (src.indexOf('BSD') !== -1) return 'BSD';
            if (src.indexOf('GNU') !== -1) return 'GNU';
            if (src.indexOf('Apache') !== -1) return 'Apache';
          })
          .catch(err => {
            console.log('fail!')
            return new Promise((resolve, reject) => {
              console.log('try to find in *.pom in sources');
              fs.readdir(path.resolve('fetch', href), (error, files) => {
                if (error) reject(error)
                const file = files.filter(file => file.indexOf('.pom') !== -1)[0];
                if (!file) return console.log('no .pom') ^ reject(new Error('no .pom'));
                fs.readFile(path.resolve('fetch', href, file), (error, data) => {
                  console.log('.pom obtained, data =', data);
                  parseXml(data, (error, res) => {
                    if (error) return reject(error);
                    console.log('parseXml =', JSON.stringify(res));
                    try {
                      const lic = ((((res.project || {}).license || [])[0] || {}).name || [])[0] ||
                        (((((((res.project) || {}).licenses || [])[0] || {}).license || [])[0] || {}).name || [])[0];
                      if (lic) return resolve(lic);
                    } catch (e) {
                      console.log('e =', e)
                    }
                    const src = String(data);
                    if (src.indexOf('MIT') !== -1) return resolve('MIT');
                    if (src.indexOf('BSD') !== -1) return resolve('BSD');
                    if (src.indexOf('GNU') !== -1) return resolve('GNU');
                    if (src.indexOf('Apache') !== -1) return resolve('Apache');
                  })
                })
              })
            })
          })
          .catch(err => {
            console.log('fail!');
            console.log('try to find in .pom from search');
            return get(pomUri)
              .then(data => {
                console.log('.pom from uri obtained, data =', data)
                fs.writeFileSync(path.resolve('fetch', pomHref), data);
                return new Promise((resolve, reject) => {
                  parseXml(data, (error, res) => {
                    if (error) return reject(error);
                    console.log('parseXml =', JSON.stringify(res));
                    try {
                      const lic = ((((res.project || {}).license || [])[0] || {}).name || [])[0] ||
                        (((((((res.project) || {}).licenses || [])[0] || {}).license || [])[0] || {}).name || [])[0];
                      if (lic) return resolve(lic);
                    } catch (e) {
                      console.log('e =', e)
                    }
                    const src = String(data);
                    if (src.indexOf('MIT') !== -1) return resolve('MIT');
                    if (src.indexOf('BSD') !== -1) return resolve('BSD');
                    if (src.indexOf('GNU') !== -1) return resolve('GNU');
                    if (src.indexOf('Apache') !== -1) return resolve('Apache');
                  })
                })
              })
          })
      })
      .catch(error => {
        console.log('getLic error =', error.message);
      })
  return cacheLic[href];
}

const reduceLics = (lics) => {
  return lics[0];
}

const getLicByDocs = fileDoc => {
  return Promise.all(fileDoc.docs.slice(0, 1).map(doc => getLic(doc)))
    .then(lics => {
      console.log('lics =', lics);
      const lic = reduceLics(lics);
      console.log('lic =', lic);
      if (!lic && !(fileDoc.shifts >= MAX_SHIFTS)) {
        fileName = shiftFileName(fileDoc.fileName);
        console.log('!lic fileName =', fileName);
        if (fileName) {
          fileDoc.fileName = fileName;
          fileDoc.shifts = (fileDoc.shifts || 0) + 1;
          return getDocsByFileName(fileDoc)
            .then(fileDoc => getLicByDocs(fileDoc));
        }
      }
      fileDoc.lic = lic || 'N/A';
      return fileDoc;
    })
};

app.post('/api/v1/lics', (req, res) => {
  const start = new Date().getTime();
  console.log('req.body =', req.body);
  const fileNames = req.body.fileNames;
  const fileHash = {};
  fileNames.forEach(fileName => fileHash[fileName] = true);
  const fileDocs = Object.keys(fileHash)
    .filter(fileName => fileName)
    .map(fileName => ({
      initialFileName: fileName,
      normFileName: normFileName(fileName),
      //fileName: normFileName,
      fileName
    }));
  return Promise.resolve(fileDocs)
    .then(fileDocs => {
      console.log('1. fileDocs =', fileDocs);
      return Promise.all(fileDocs.map(getDocsByFileName));
    })
    .then(fileDocs => {
      console.log('2. fileDocs =', fileDocs);
      return Promise.all(fileDocs.map(getLicByDocs));
    })
    .then(fileDocs => {
      console.log('3. fileDocs =', fileDocs);
      res.json({
        success: true,
        fileDocs,
        lics: fileDocs.map(fileDoc => fileDoc.lic),
        time: new Date().getTime() - start
      });
    })
    .catch(error => {
      console.log('catch error =', error.message);
      res.json({
        success: false,
        error,
        fileDocs,
        lics: fileDocs.map(fileDoc => fileDoc.lic || 'N/A'),
        time: new Date().getTime() - start
      })
    })
});

app.listen(PORT, () => {
  console.log('server listening on port', PORT);
});
